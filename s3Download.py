import requests,os
import os
from infer_frcnn import  infer_from_model
def downloadFile(data):
    # s3=boto3.client('s3')
    # pickle_path = data['pickle_path']
    # model_path = data['model_path']
    file_name=data["file_name"]
    # file_names=s3.file_names_objects(Bucket='annotatedimagescams')['Contents']
    # print(file_names)
    # s3_object = file_name.split("/")[3]+"/"+file_name.split("/")[4]
    # print(s3_object)
    # if s3_object is not None:
    name = file_name.split("/")[-1]
    print(name)
    try:
        image_path = "./images"+os.path.sep+name
        f = open(image_path,"wb")
        f.write(requests.get(file_name).content)
        f.close()
        # s3.download_file('annotatedimagescams', s3_object, f"images/{name}" )
    except Exception as e:
        print(e)
    # else:
    #     import os
    #     if not os.path.exists(s3_object):
    #         os.makedirs(s3_object)
    coordinates=infer_from_model(data)
    
    return  coordinates[0]
