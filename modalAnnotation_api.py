from flask import Flask,request,jsonify
from s3Download import downloadFile
from flask_cors import CORS, cross_origin
import logging
import os
from s3Download import  downloadFile
import json

os.makedirs('logs',exist_ok=True)
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
logging.basicConfig(filename='logs/modal_annotation.log', 
                    format='%(asctime)s %(message)s', 
                    filemode='a',
                    level = logging.INFO)
logger=logging.getLogger()

app =Flask(__name__)
cors = CORS(app)

@app.route('/modal-annotation',methods=['POST'])
def modal_annotation_api():
    try:
        if request.method=='POST':
            logger.info(f'Recieved request from http://{request.remote_addr}')
            text = request.get_json()
            logger.info(f'requestParameter{text}')
            logger.info(f'The recieved text is {(text)}')
            for path in os.listdir("./images"):
                full_path = os.path.join("./images", path)
                if os.path.isfile(full_path):
                    os.remove(full_path)
            
            return jsonify(downloadFile(text))
    
    except Exception  as e:
        for path in os.listdir("./images"):
            full_path = os.path.join("./images", path)
            if os.path.isfile(full_path):
                os.remove(full_path)
            print(str(e))
            return json.dumps({
                "error":True,
                "message":"Cannot process file"
                    }),500     

if __name__ == '__main__':
    if('PORT' in os.environ):
        app.run(host="0.0.0.0",port=os.environ['PORT'],debug=True)
    else:
        logging.warning('Port number is not set.')
# remote_addr