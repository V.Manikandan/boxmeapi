FROM ubuntu
WORKDIR /app
# RUN apt install openjdk-8-jdk openjdk-8-jre -y
# RUN apt-get update && \
#     apt-get install -y openjdk-8-jdk 
RUN apt-get update &&  apt-get install -y python3 && apt-get install -y python3-pip
RUN apt-get install -y libsm6 libxext6 libxrender-dev libglib2.0-0
# RUN pip install opencv-python
RUN pip3 install virtualenv
RUN apt-get install -y vim
RUN apt-get install -y less
RUN virtualenv modalAnnotation_api -p python3
RUN cd modalAnnotation_api && . ./bin/activate
RUN pip3 install gunicorn
COPY ./su_requirements.txt /app/modalAnnotation_api/
RUN mkdir -p /app/modalAnnotation_api/hdfc_segment_model
RUN mkdir -p /app/modalAnnotation_api/keras_frcnn
RUN mkdir -p /app/modalAnnotation_api/images
COPY ./hdfc_segment_model /app/modalAnnotation_api/hdfc_segment_model
COPY ./keras_frcnn /app/modalAnnotation_api/keras_frcnn
COPY ./images /app/modalAnnotation_api/images
COPY ./__All_Errors.txt /app/modalAnnotation_api/
RUN pip3 install -r /app/modalAnnotation_api/su_requirements.txt
COPY ./*.py /app/modalAnnotation_api/
RUN ls /app/modalAnnotation_api/
CMD ["python3", "modalAnnotation_api.py"]

